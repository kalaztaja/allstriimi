# ALL Striimi - ohje

## Kerran tehtävät toimenpiteet

1. Asenna OBS https://obsproject.com/fi/download
2. Avaa OBS asennuksen jälkeen
   1. Settings->Stream->Stream Key
   2. Valitse Service Twitch
   3. Syötä Stream Keyn saat kysymällä @kalaztaja tai @aivoapina
   4. HUOM! Jos stream key on vaihdettu, ei avain toimi. Tällöin kysy uutta hallitukselta
3. Laita tämän dokumentin ohessa tulleet tiedostot sinulle sopivaan paikkaan
4. Pyydä editor-oikeudet twitch-kanavalle @kalaztaja tai @aivoapina
5. Kysy kredentiaalit palveluun varjola.dy.fi/dashboard @aivoapina

#### HUOM! Jos importtaat scenet voit skipata kohtia ja korvata jokaisen ”add new” kohdan sillä, että valitset sitä vastaavan sourcen ja right clickin avulla muutat sen propertyjä ohjeen mukaan.

## Yleiset asetukset

Näitä ei tarvitse tehdä uudestaan ellet muuta koneen asetuksia.

1. Aseta audio-asetukset välilehdeltä Settings->Audio
   Helppo setup on niin, että sinulla on ääninä sinun koneesi ääniulostulo ja mikrofonisi. Tällöin striimi kuulee sen kaiken, minkä itse kuulet ja mitä itse sanot. Kehittyessäsi voit alkaa käyttämään erilaisia miksereitä (löytyy internetistä googlaamalla)
   Kuitenkin OBS automaattisesti antaa vähän huono lähtökohdat säännellä audiota. Disabloidaan ne automaattiasetukset niin, että on helpompi kustomoida skenekohtaisesti audioita.
2. Aseta siis Desktop Audio->Disable ja Mic/Auxiliary Audio-> Disable. Muut vaihteohdot myös disabled.
3. Kohdasta Output aseta Video Bitrate->6000 ja Encoder->Software.
   6k bitrate on se, minkä Twitch vastaanottaa. Ulostulo riippuu kaistan leveydestä.
4. Halutessasi voit lisää kaikki tarvitsevat Hotkeyt hotkey-valikosta, mutta ne ei ole välttämättömät

## Skenien tekeminen

Skenien setuppaamisessa suosittelen, että et käytä studio modea, koska silloin kaikki lisäämäsi asiat toimivat niin kuin ne toimisivat striimin aikana. Striimin pyörittämisessä sitten suosittelen ainoastaan käyttämään studio modea.
Jokaisessa numerossa tehdään yksi scene. Voit lisätä scenejä vasemmasta alanurkasta painamalla +-nappulaa ja laittamalla sceneille haluamasi nimi. Jokaisessa scenessä lisäämme asioita sources-kohdassa. Voit lisätä sceneen sourceja helposti valitsemalla scene ja lisäämällä sourceja.
Monessa kohtaa ei ole niin tärkeätä, että mitä sourcen tyyliä käytät. Esimerkiksi kuvaa pystyt tuomaan näytölle monella eri tapaa ja joistakin niistä toimii paremmin toisessa ympäristössä kuin toisessa. Tästä esimerkki on, että voit taltioida League of Legends clienttiä window capturen, game capturen tai vaikka display capturen avulla. Käytä vaan sitä, mikä sinulla toimii, vaikka ohjeessa sanottaisiinkin toisin. Testaamisella oppii!

1. Ensimmäinen skene on introskene (yksinkertaisin kaikista)

   1. Lisää Media Source->Add new->Browse local file->Valitse Scene 1 – Intro kansiosta video
      - Muista laittaa Loop päälle
      - Muut asetukset saat valita itse (ite suosin ”restart playback..” ja ”show nothing..” päälle)
      - Varmista, että introvideo peittää koko näytön
   2. Lisää Add new->Browser Source->Url löytyy osoitteesta https://varjola.dy.fi/dashboard/#graphics
      - Kredentiaalit saat kysymällä @aivoapina tai @kalaztaja
      - Width 1920 ja height 1080
      - Lisätietoja kellon ohjaamisesta striimin ohjauksessa

2. Toinen skene on champion select / league of legends client scene

   1. Lisää League of Legends client. Ite käytin window capturea ja otin kursorin seuraamisen pois
   2. Asettele se haluamallasi tavallasi näytölle. Itse suosin sitä, että se peittää koko scenen
   3. Lisää Image Slideshow
      - Time between slides 10000
      - Transition speed vaikka 1400
      - Loop päälle
      - Valitse transitionkuvat kansiosta Scene 2 – Champion Select kansiosta
   4. Lisää audio output capture->Create New (nimeä Desktop)->Default
      - Se varmistaa, että se mitä tulee defaulttina ulos sun koneestasi menee striimiin
      - Eli pitäisi vastaa sitä, mitä sä kuulet
      - Säädä Audio MIxeristä Desktop niin, että se ei käy punaisella, kun musiikki soi
      - Saa kuitenkin käydä -20 kohdalla helposti
   5. Lisää audio input capture->valitse mikrofonisi ja vaihda siihen nimi
      - Säädä samalla tapaa mikrofonisi, että kun puhut niin se kuuluu maksimissaan yhtä kovaa kuin musat
      - Tässä on tärkeää testata, että muiden puheääni kuuluu discordin kautta yhtä kovaa
      - Voit säätää audiota painamalla audio sliderin viereästä advanced audio properties ja säätää volumeen joko lisää tai enemmän volumea.

3. Kolmas skene on Spectating League of Legends

   1. Lisää Game Capture->Add new (Loli)->
      - Capture specific Window
      - Valitse game client
      - Ota capture cursor pois päältä
   2. Lisää BottomLeftBar Add Image->Add new-> valitse kuva Scene 3 – Spectating
      - Säädä se niin, että kun spekkaat peliä se peittää juuri ja juuri sen vasemman alanurkan palkin (sen palkin koko riippuu sinun settingseistä lolissa)
      - Voit säätää kuvan koko vaan normaalisti venyttämällä
   3. Lisää overlay loliin Add Browser source-> Url löytyy osoitteesta https://varjola.dy.fi/dashboard/#graphics
      - Kredentiaalit saat kysymällä @aivoapina tai @kalaztaja
      - Width 1920 ja height 1080
      - Lisätietoja striimin ohjaamisessa
   4. Lisää audiot tähän sceneen samalla tapaa kuin lisäsit ne client-kohdassa

4. Neljäs skene on caster-skene

   1. Lisää taustavideo Media Source->Add new-> Scene 4 – kansiosta palkkivideo
      - Loop päälle
      - Restart palyback when source becomes active päälle
      - Voit säätää nopeutta, miten haluat
      - Paina ok, ja sen jälkee paina sources kohdassa oikealla juuri lisäämäsi palkkivideota
      - Valitse Filters->Effect Filters->Add new->Color Correction->Säädä brightnessia kuten haluat, että palkkivideo ei ole niin silmiin pistävä
   2. Listää castebarit Browser Source->Add new-> Url löytyy osoitteesta https://varjola.dy.fi/dashboard/#graphics
      - Kredentiaalit saat kysymällä @aivoapina tai @kalaztaja
      - Width 800 ja height 600
   3. Jos haluat lisätä discordin, lisää window capture ja discord
      - Tässä joudut todennäköisesti croppaamaan ja säätämään aika paljon
      - Cropata voit painamalla alttia pohjassa, kun vedät reunoista
      - Huomaa, että jos liikutat discordia, liikkuu myös tämä objecti
      - Vastaavasti, jos discordi ei ole esillä näytöllä, ei välttämättä obs osaa recordaa puhekuplia vaan jäädyttää ne
   4. Lisää audiot tähän sceneen samalla tapaa kuin lisäsit ne client-kohdassa

5. Viimeisenä ei lisätä enää sceneä, mutta lisätään musiikkisi
   1. Jos haluat niin voit kuunnella vain itse musiikkiasi haluamallasi tavalla niin, että kuulet sen itsekkin, milloin striimikin kuulee sen
   2. Poikkeuksena tässä on introscene. Voit joko lisätä siihen desktop-audion, jolloin se toimii samoin kuin muutkin scenet. Tällöin sinun täytyy mutea vain discordi ja muut sovellukset, että ne ei kuulu introscenen aikana
   3. Vaihtoehtoisesti lisää yksittäinen erillisen audio mixer softan avulla tai käytä jotain audiofilua media sourcen muodossa

Tämän lisäksi voit lisätä niin paljon scenejä kuin vain haluat. Uskon, että tässä kohdassa osaat jo itsekkin hyvin.

## Tranistion animaatioiden lisääminen

Siellä on monta hyvää transitionia, joista voit valita itelles parhaat. Suosittelen vähintään kahta eriä. Voit lisätä ne kaikki samalla tavalla

1. Oikealla alhaalla näet Scene transitions. Paina plussaa
2. Valitse stinger->Nimeä se->Valitse Tranistion-kansion video filuista haluamasi
3. Säädä transition point niin, että se vaihtuu sopivasti. Haluat, että uusi scene ei näy ennen kuin animaatio on pyörähtänyt, mutta se ei vaan töksähdä siihen sen jälkeen
4. Hyvä arvaus on 1000ms melkein jokaisen animaation kohdalla
5. Paina ok. Voit testata transitionia menemällä studio modeen->painaa keskeltä quick transitionsin vierestä plussaa lisätäksesi animaatiosi
6. Jos näyttää hyvältä se on hyvä siinä ja lisää vaa lisää

## Ennen striimin aloittamista

1. Avaa osoite https://varjola.dy.fi/dashboard/#
   a. Kirjaudu sivulle kredentiaaleilla, jotka saat @aivoapina tai @kalaztaja
   b. Siinä on sun sekä spekkauksen overlayn, kellon ja castereiden ohjauspaneelit
   c. Tutustu siihen. Main workspacesta T3G Casters ja scorebar vastaa casterbaria ja spekkauksen scorebaria.
   d. Countdown kellosta paina Run To, kun haluat vaihtaa aikaa
2. Sovi joukkueiden kapteenien kanssa etukäteen, että tulette spekkaamaan
   a. Varmista, että sekä sinä että juontajat tulee invatuksi spekkattavaan peliin
3. Laita kalaztajalle mainosviesti
   a. Mainosviesti englanniksi ja tyyli vapaa
4. Sano jollekkin twitch tunnarit omaavalle (kuten kalalle), että lisää sinun twitch-käyttäjän editoriksi
   a. Tämän jälkeen mene osoitteeseen https://dashboard.twitch.tv/u/tr3gamers/stream-manager
   b. Vaihda title, category, stream language ja tags tarvittaessa
   c. Muista painaa DONE
5. Muista sopia juontajien kanssa kuviot
6. Laita valitsemasi biisilista valmiiksi (kala käytti omaansa spotifitystä, löytyy nimellä ”Bob-omb Battlefield”)
7. Päivitä overlayhin castereiden nimet ja tägit sekä joukkueet valmiiksi
8. Laita kelloon vaan aika, kun striimin on tarkoitus alkaa
9. Käy striimin käsikirjoitus lävitse

## Striimin pyörittäminen

1. Käsikirjoitus menee aina seuraavanlaisesti
   a. Introkuva päälle 15 min ennen striimin alkamista
   b. Siitä studioon, studiosta selectiin, selectistä peliin, pelistä studioon, mistä taas joko tauolle tai selectiin
   c. Lopulta kiitä striimistä ja pistä introfilmin kanssa ulos.
2. Muista päivittää overlayt https://varjola.dy.fi/dashboard/#
3. Muista koordinoida tiimien ja juontajien kanssa
   a. Protip! Voit puhua selostajille ilman, että striimi kuulee, jos muteet obsissa mikrofonisi ja puhut discordissa. Selostajat saattavat mennä tästä vähän sekaisin, joten kannattaa varoittaa.
4. Muista pitää taukoja ja pitää myös hauskaa! Tämä on turnauskestävyyslaji ja helposti voi väsähtää, milloin alkaa striimin pyörittäminenkin olemaan vaikea

## Bonusjutut

Lataa twitch client ja käy laittamassa sieltä League of Legends extension päälle. Löydät ohjeet siihen osoitteesta https://dashboard.twitch.tv/u/tr3gamers/stream-manager

Kannattaa transitioneita käyttää maltillisesti. Jos on kiire esim studiosta selectiin niin käytä vaan fadea.

Käytä aina studio modea. Silloin voit viilata pilkkuja ja tarkastaa tulevia skenejä samalla, kun striimi pyörii.

Huomioi kuitenkin, että audio mixer näyttää aina ulostulevan audion eikä nykyistä audiota. Samalla voin muistuttaa, että videot näkyvät ainoastaan aktiivisessa scenessä eli scenessä, joka näkyy ulos.

Muista pitää hauskaa. Kuten aikaisemminkin sanoin, tää käy nopeesti tylsäksi, jos tästä ei nauti.

###### Jos on kysyttävää tai haluat jeesiä, vastaan aina telegramissa osoitteessa @kalaztaja.
